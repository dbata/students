# UniverSIS-students Installation

## Prerequisites

Node.js version >6.14.3 is required. Visit [Node.js](https://nodejs.org/en/)
and follow the installation instructions provided for your operating system.

## Install dependencies

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8. To **install angular-cli** execute

`npm install -g @angular/cli@6`

You may need to run it as root.


## Deploy
### Development

1. Clone the application and redirect to the project folder 
    ```console
    $ git clone git@gitlab.com:universis/universis-students.git
    $ cd universis-students/
    $ git submodule update --init --remote
    ```
2. Install the dependencies
    ```console
   $ npm ci
    ```
3. Run `ng serve` for a devevelopment server
- Navigate to `http://localhost:7001/`

The app will automatically reload if you change any of the source files.

### Production
1. Clone the application and redirect to the project folder 
    ```console
    $ git clone git@gitlab.com:universis/universis-students.git
    $ cd universis-students/
    $ git submodule update --init --remote
    ```
2. Install the dependencies
    ```console
   $ npm ci
    ```
3. Configuration

  `cp src/assets/config/app.json src/assets/config/app.production.json`

  Add the appropriate values for client_id, client_secret and URLs in `src/assets/config/app.production.json`

- Make sure you build from the source code after every change

    `npm run build`

- Use pm2 to manage the application
  * start

    `pm2 start pm2.config.json`

    Navigate to `http://localhost:7001/`

  * check the status

    `pm2 status universis_students`

  * stop

    `pm2 stop universis_students`
    
### Testing
We use Jasmine/Karma to write and run tests for all our projects. Tests are executed on ChromeHeadless using puppeteer. Puppeteer is listed as a peerDependency and must be installed in the following way:
1. Navigate to the directory with (all) your project(s) which is typically done with `cd ..` The tree should look something like:  
<pre>
├── universis
│   └── node_modules
├── universis-students
│   └── node_modules
└── universis-teachers
    └── node_modules
</pre>
2. Run `npm install puppeteer`. Your tree should now look like:
<pre>
├── universis
│   └── node_modules
├── universis-students
│   └── node_modules
├── universis-teachers
│   └── node_modules
└── node_modules
    └── puppeteer
</pre>
3. You should now be able to navigate to a project directory such as `cd universis-students` and run tests with either of two options:
-
  - `npm run test` will launch Karma in a Chromium browser, will watch for your changes and run tests on save. To kill this you have to ^C on terminal, or close Chromium from the UI **three times**. Use this for **development**.
  - `npm run test-ci` will launch Karma in a headless browser, will **NOT** watch for changes and will exit on completion. Use this for **CI**.

## Executable Creation
After the application is built with `npm run build`, it can be packaged into a
single executable file that contains both the server and the static files with
the use of `pkg`.

1. Install pkg
```
npm npm install -g pkg
```
2. Run `pkg` with `standalone.js` as the entry point

**Make sure** that `dist/` does not contain an `app.production.json`, since this
file will be created outside of the binary on the first run of the server (or
can be created manually).
```
pkg standalone.js -t node16-linux-x64 --no-bytecode --public-packages "*" --public -c package.json
```
Options:
- `--no-bytecode` to stay cross-platform
- `--public-packages "*"` and `--public` because some dependencies have a
  broken license property in their package.json
- `-c package.json` to get the config that points to the static assets
  directory (`"pkg"` property on package.json)
- `-t node16-[platform]-[arch]` to specify the target system(s)

For more information and options about the packaging, check the
[pkg documentation](https://github.com/vercel/pkg#usage).

3. After the binary has been created, you can run it either directly or with
the `pm2` process manager.

### Running the executable
Apart from the configuration file and the logs, the executable is completely
self-contained, so there is no need to have anything other installed on the
server.

#### Config file location
The location of the configuration file (app.production.json) can be passed to
the executable in the following ways:
- By using the `--config` command line parammeter when running the application.
  For example,
  ```
  ./universis-students --config config/app.production.json
  ```
- Similarly with the shortened version `-c`.
- By setting the `STUDENTS_APP_CONFIG` environment variable.

Those parameters will be processed in the above order. For example, if
`--config` is present, `-c` and any other command line arguments (except those
for the log directory, presented in the next section) will be ignored, and if
any of the command line arguments are given, the environment variable will be
ignored.

The paths can be either relative or absolute.

The default location if no location is given is `app.production.json` inside
the current working directory when the process runs.

If a config file is not found on the specified path, the application will exit.
In order to create a config file with default values at the current working
directory, run the executable without any parameters (cli or env variable).
This file then needs to be configured accordingly.

If the file is moved, the executable must be run again with the new location
passed.

#### Log directory
The log directory can be similarly configured by using the `--log` or `-l` cli
arguments, or by setting the `STUDENTS_APP_LOG` environment variable.

If the directory does not already exist, it will be created. If no parameter
is set, it will be created at the current working directory.

