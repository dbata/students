import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {CardCollapsibleComponent} from './components/card-collapsible/card-collapsible.component';
import {FormCollapsibleComponent} from './components/form-collapsible/form-collapsible.component';

@NgModule({
    imports: [
        CommonModule,
        TranslateModule
    ],
    declarations: [
        FormCollapsibleComponent,
        CardCollapsibleComponent
    ],
    providers: [
    ],
    exports: [
        FormCollapsibleComponent,
        CardCollapsibleComponent
    ],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA
    ]
})
export class ElementsModule {

}
