import { Component, OnInit, ViewChild } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ErrorService, LoadingService } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProfileService } from '../../profile/services/profile.service';

import { ToastrService } from 'ngx-toastr';
import { AdvancedFormComponent } from '@universis/forms';

@Component({
  selector: 'app-apply',
  templateUrl: './apply.component.html',
})
export class ApplyComponent implements OnInit {
  public student?: any;
  public loading = true;
  public src = 'PreferredSpecialtyRequestActions/new';
  public continueLink = ['/requests', 'list'];
  public data = {};
  public preferredSpecialtyRequestAction?: any;
  public currentSpecialtyName?: string;

  @ViewChild('form') form?: AdvancedFormComponent;
  constructor(
    private _context: AngularDataContext,
    private _translateService: TranslateService,
    private _router: Router,
    private _toastService: ToastrService,
    private _loadingService: LoadingService,
    private _profileService: ProfileService,
    private _errorService: ErrorService,
    private _activatedRoute: ActivatedRoute
  ) {}

  showLoading(loading: boolean) {
    this.loading = loading;
    if (loading) {
      this._loadingService.showLoading();
    } else {
      this._loadingService.hideLoading();
    }
  }

  async ngOnInit() {
    try {
      this.showLoading(true);
      const student = await this._profileService.getStudent();
      // only if student follows a specialty other than the core specialty
      if (student && student.specialtyId && student.specialtyId > -1) {
        // validate specialty (readonly)
        if (typeof student.specialty === 'string' && student.specialty.length) {
          // display current specialty name (request is unavailable)
          this.currentSpecialtyName = student.specialty;
        } else {
          // fetch current specialty name to display it
          const currentStudentSpecialty: {name: string} | undefined = await this._context
            .model('students/me/specialty')
            .select('name')
            .getItem();
          this.currentSpecialtyName = currentStudentSpecialty && currentStudentSpecialty.name;
        }
      }
      const currentYear = student && student.department && student.department.currentYear && student.department.currentYear.id;
      const currentPeriod = student && student.department && student.department.currentPeriod && student.department.currentPeriod.id;
      this.preferredSpecialtyRequestAction = await this._context
        .model('PreferredSpecialtyRequestActions')
        .where('academicYear')
        .equal(currentYear)
        .and('academicPeriod')
        .equal(currentPeriod)
        .and('student')
        .equal(student && student.id)
        .orderByDescending('dateCreated')
        .getItem();
      if (this.preferredSpecialtyRequestAction != null) {
        return this._router.navigate([
          '/requests',
          'PreferredSpecialtyRequestActions',
          this.preferredSpecialtyRequestAction.id,
          'preview',
        ]);
      } else {
        Object.assign(this.data, {
          academicYear: student.department.currentYear,
          academicPeriod: student.department.currentPeriod,
          student: student.id,
          name: this._translateService.instant(
            'PreferredSpecialtyRequestActions.PreferredSpecialtyRequest'
          ),
          studyProgram: student.studyProgram
        });
        this.showLoading(false);
      }
    } catch (err) {
      console.error(err);
      this._errorService.navigateToError(err);
    } finally {
      this.showLoading(false);
    }
  }

  async onCompletedSubmission($event: any) {
    let continueLink;
    if (this.continueLink && Array.isArray(this.continueLink)) {
      continueLink = this.continueLink.map((x) => {
        return x;
      });
    }
    if (continueLink) {
      await this._router.navigate(continueLink);
      this._toastService.show(
        $event.toastMessage.title,
        $event.toastMessage.body
      );
    } else {
      await this._router.navigate(['../'], {
        relativeTo: this._activatedRoute,
      });
      this._toastService.show(
        $event.toastMessage.title,
        $event.toastMessage.body
      );
    }
  }
}
